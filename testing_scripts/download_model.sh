#!/bin/bash
fileid="1KdrEavZkFtm5IrKk8ngK9OrCE8LUaoV9"

filename="../src/models/best_hybrid_model_vgg19.zip"
curl -c ./cookie -s -L "https://drive.google.com/uc?export=download&id=${fileid}" > /dev/null 
curl -Lb ./cookie "https://drive.google.com/uc?export=download&confirm=`awk '/download/ {print $NF}' ./cookie`&id=${fileid}" -o ${filename} &&\
unzip ../src/models/best_hybrid_model_vgg19.zip -d ../src/models/
