#%%
import time
import os
import sys
sys.path.append(os.path.abspath(os.path.join(__file__,"../")))  
sys.path.append(os.path.abspath(os.path.join(__file__,"../..")))
from video_segmentation import segment_frames
from evaluate_video import evaluate_video

start_time=time.time()
#%%
calibrated_video_name='IMG_0874_calibration.MOV'
video_name='IMG_0875.mp4'
standard_height=60.5   # inches
wrist='right_handed'
#%%
fps=60
#%%
path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'data','testing_videos')
calibrated_video_path=os.path.join(path,calibrated_video_name)
inches_in_pixel,lowest_point,x_mean=segment_frames(standard_height,calibrated_video_path)
video_path=os.path.join(path,video_name)
evaluate_video(file_path,wrist,fps,inches_in_pixel,lowest_point,x_mean)
end_time=time.time()
print('Code execution complete..\n')
print('Total execution time is : %d minutes and %d seconds'%(((end_time-start_time)-(end_time-start_time)%60)//60,(end_time-start_time)-((end_time-start_time)-(end_time-start_time)%60)))
