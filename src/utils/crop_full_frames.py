#%%
import cv2
import numpy as np
import os
import sys
import shutil
import gluoncv
import gc
from gluoncv import model_zoo, data, utils
sys.path.append(os.path.abspath(os.path.join(__file__,"../")))
#%%
""" Crops the largest human figure in each frame using detection algorithm.
    'file_path' should point to the main directory containing the dataset,
    within different classes folder.
    'target_path' would create a new folder, with cropped images saved into
    their actual classes."""
#%%
def get_model():
    #detector = model_zoo.get_model('faster_rcnn_resnet50_v1b_voc', pretrained=True)
    detector = model_zoo.get_model('faster_rcnn_fpn_resnet50_v1b_coco', pretrained=True)
    detector.reset_class(["person"], reuse_weights=['person'])
    detector.nms_thresh=0.35
    detector.post_nms=2
    return detector
#%%
detector=get_model()
pad_factor=0.2
#%%
def get_bounding_box(full_path,h,w):

    x, trans_image = data.transforms.presets.rcnn.load_test(full_path,short=512)
    class_IDs, scores, bounding_boxs = detector(x)
    index,BB=calculate_max_area(bounding_boxs,scores)
    if index is None:
        return None
    ha=h/trans_image.shape[0]
    wa=w/trans_image.shape[1]
    x1,y1,x2,y2=(BB*np.array([ha,wa,ha,wa])).astype(int)
    return x1,y1,x2,y2
#%%
def calculate_max_area(bounding_box,scores):
    bounding_box=bounding_box.asnumpy()
    scores=scores.asnumpy()
    area=0
    index=None
    for i in range(np.sum(scores>0.5)):
      x1,y1,x2,y2=bounding_box[0][i]
      width=int(x2-x1)
      height=int(y2-y1)
      if width*height>area:
        area=width*height
        index=i
    return index,bounding_box[0][index]
#%%
def crop(img_path):
    img=cv2.imread(img_path)
    BB=get_bounding_box(img_path,img.shape[0],img.shape[1])
    if BB is None:
        return None
    x1,y1,x2,y2=BB
    width=int((x2-x1)*pad_factor)
    height=int((y2-y1)*pad_factor)
    # return img[y1:y2,x1:x2]
    return img[np.max([0,y1-height]):y2+height,np.max([0,x1-width]):x2+width,:]


#%%
if __name__=='__main__':
    file_path=os.path.join(os.path.abspath(os.path.join(__file__,"../")),'full_classes')
    # file_path=('full_classes')
    target_path=os.path.join(os.path.abspath(os.path.join(__file__,"../")),'cropped_classes')
    print(file_path)
    print(target_path)
    if os.path.exists(target_path):
         shutil.rmtree(target_path)
    os.makedirs(target_path)
    print('Total classes detected : {}'.format(os.listdir(file_path)))
    for classes in os.listdir(file_path):
        print('For class: {}'.format(classes))
        class_path=os.path.join(file_path,classes)
        if os.path.exists(os.path.join(target_path,classes)):
          shutil.rmtree(os.path.join(target_path,classes))
        os.makedirs(os.path.join(target_path,classes))

        for img_path in os.listdir(class_path):
            print('Cropping ',os.path.join(class_path,img_path))
            cropped_image=crop(os.path.join(class_path,img_path))
            if cropped_image is not None:
              cv2.imwrite(os.path.join(os.path.join(target_path,classes),'{}'.format(img_path)),cropped_image)
              del cropped_image
              gc.collect()