#%%
import os
import sys
import cv2
import imutils
sys.path.append(os.path.abspath(os.path.join(__file__,"/..")))
sys.path.append(os.path.abspath(os.path.join(__file__,"../..")))
from gluoncv import model_zoo, data
import mxnet as mx
import numpy as np
import shutil
from gluoncv.utils.viz import get_color_pallete
import pandas as pd
from utils.frames_extraction import extract
from utils import paths
import re
import json
import argparse
import concurrent.futures
import multiprocessing
import gc
import time
import parser_arguments
#%%
def generate_json(data,json_path):
    if os.path.exists(json_path):
        os.remove(json_path)
    with open(json_path, 'w') as fp:
        print('Writing JSON file  ',json_path)
        json.dump(data, fp,indent=2)
    print('JSON file generated !')
#%%
def reject_outliers(data, m=2.):
    d = np.abs(data - np.median(data))
    mdev = np.median(d)
    s = d / (mdev if mdev else 1.)
    return s < m
#%%
class segmentation:
    def __init__(self):
        self.net=self.get_models()
        self.class_index=15
        self.thresh=6
        # self.stick_height=stick_height_inches
        # self.ext='.png'

    def get_models(self):
        net = model_zoo.get_model('deeplab_resnet101_coco', pretrained=True)
        # net = model_zoo.get_model('deeplab_resnet152_coco', pretrained=True)

        return net


#%%
ext='.png'
border=50
extract_path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'data','extracted_frames')
target_path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'results','segmented_frames')
#%%
def person_segment(full_path):
        
        print('Segmenting ',full_path)
        img_path=os.path.split(full_path)[-1]
        image=cv2.imread(full_path)
        original_height,original_width=image.shape[0],image.shape[1]
        image = mx.ndarray.array(image)
        image=image_resize(image,new_height=original_height//3)
        # image=image_resize(image,new_height=256)
        segment=segmentation()
        img = data.transforms.presets.segmentation.test_transform(image,ctx = mx.cpu(0))
        output = segment.net.predict(img)
        # try:
        predict = mx.nd.squeeze(output[:,segment.class_index]).asnumpy()
        # except mx.MXNetError:
        p=np.where(predict<segment.thresh,0,1)
        del segment, output,predict
        gc.collect()
        p=p.astype('uint8')
        cnts = cv2.findContours(p.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        mask = np.ones(p.shape[:2], dtype="uint8") * 255
        # loop over the contours
        max_area_cnt=max(cnts,key=cv2.contourArea)
        max_area=cv2.contourArea(max_area_cnt)
        for c in cnts:
            # if the contour is bad, draw it on the mask
            cnt_area=cv2.contourArea(c)
            if cnt_area<max_area:
                cv2.drawContours(mask, [c], -1, 0, -1)
            # remove the contours from the image and show the resulting images
        img = cv2.bitwise_and(p,p, mask=mask)
        cnts = cv2.findContours(img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        max_area_cnt=max(cnts,key=cv2.contourArea)
        x,y,w,h = cv2.boundingRect(max_area_cnt)
        mask = get_color_pallete(img, 'coco')
        mask.save(os.path.join(target_path,'{}'.format(img_path.split('.')[0]+ext)))
        del img, mask
        gc.collect()
        ha=image.shape[0]/original_height
        wa=image.shape[1]/original_width
        image=image_resize(image,original_height)
        image=image.asnumpy()
        x,y,w,h =([x,y,w,h]/np.array([wa,ha,wa,ha]))
        x1,y1,w1,h1=np.array([x,y,w,h]).astype(int)
        image = cv2.rectangle(image,(x1,y1),(x1+w1,y1+h1),(0,255,0),2)
        lowest_point=h1+y1
        x_mean=x+(w/2)
        cv2.line(image,(0,lowest_point+border),(image.shape[1]-1,lowest_point+border),color=(0,255,0),thickness=2)
        cv2.line(image,(0,lowest_point-border),(image.shape[1]-1,lowest_point-border),color=(0,255,0),thickness=2)
        cv2.ellipse(image,(int(x_mean),y1+h1),(15,10),0,0,180,(0,0,255),-1)
        cv2.imwrite(os.path.join(os.path.join(target_path,img_path)),image)
        del image
        gc.collect()
        return h,y,x_mean

#%%
def image_resize(image,new_height):
    h,w=image.shape[:2]
    height_percent=new_height/h
    new_width=int(height_percent*w)
    image=data.transforms.image.imresize(image, w=new_width, h=new_height)
    return image
# %%
def segment_frames(standard_height,calibrated_video_path):
    calibrated_video_name=os.path.split(calibrated_video_path)[-1]
    json_files_path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'results',calibrated_video_name.split('.')[0])
    print('Segmented images will be stored in ',target_path)
    if os.path.exists(target_path):
         shutil.rmtree(target_path)
    os.makedirs(target_path)
    extract(calibrated_video_path,extract_path)  
    height={}
    x_point={}
    lowest={}
    status={}
    images_list = list(paths.list_images(extract_path))
    images_list.sort(key=lambda f: int(re.sub('\D', '', f)))
    images_list=images_list[::40]
    total_workers=multiprocessing.cpu_count()
    wf=0.5
    print('Total processors : ', int(total_workers*wf))
    with concurrent.futures.ProcessPoolExecutor(max_workers=int(total_workers*wf)) as executor:
        print('Starting ProcessPoolExecutor for pose estimation')
        results=executor.map(person_segment,images_list)
    print('ProcessPoolExecutor completed successfully for segmentation')
    for i,result in enumerate(results):
        img_path=os.path.split(images_list[i])[-1]
        height_bounding_box,highest_point,x_mean=result[0],result[1],result[2]
        height[img_path]=height_bounding_box
        lowest[img_path]=height_bounding_box+highest_point
        x_point[img_path]=x_mean
    all_height=np.array(list(height.values()))
    all_lowest=np.array(list(lowest.values()))
    all_x=np.array(list(x_point.values()))
    index=reject_outliers(all_height)
    frame_percent=len(all_height[~index])/(len(all_height))*100
    print('%.1f %% of the frames deviate substantially from the median pixel height'%frame_percent)
    print('Those frames are ',list(filter(lambda x: ~index[x], range(len(~index)))))
    if frame_percent>=40:
        print('Video not stable. Terminating..')
        status['Response']='Calibration unsuccessful'
        status['Remarks']='%.1f %% of the frames deviate substantially from the median pixel height'%frame_percent
        generate_json(status,json_files_path+'_status.json')
        sys.exit()
    print('Discarding abnormal frames and recalculating height')
    all_height=all_height[index]
    all_lowest=all_lowest[index]
    all_x=all_x[index]
    print(height)
    print('Mean height: ',all_height.mean())
    print('Mean lowest: ',all_lowest.mean())
    print('Mean X point: ',all_x.mean())
    inches_in_pixel=standard_height/all_height.mean()
    print('Inches per pixel : ',inches_in_pixel)
    status['Response']='Success'
    status['Inches per pixel']=inches_in_pixel
    status['lowest_point']=all_lowest.mean()
    status['x-mean_point']=all_x.mean()
    generate_json(status,json_files_path+'_status.json')
    return inches_in_pixel,all_lowest.mean(),all_x.mean()
# %%
if __name__=='__main__':
    start_time=time.time()
    args = parser_arguments.getArgs(sys.argv[1:])
    standard_height=args.person_height  # inches
    calibrated_video_name=args.video
    path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'data','testing_videos')
    calibrated_video_path=os.path.join(path,calibrated_video_name)
    path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'data','testing_videos')
    calibrated_video_path=os.path.join(path,calibrated_video_name)

    inches_in_pixel,lowest_point,x_mean=segment_frames(standard_height,calibrated_video_path)
    end_time=time.time()
    print('Total execution time is : %d minutes and %d seconds'%(((end_time-start_time)-(end_time-start_time)%60)//60,(end_time-start_time)-((end_time-start_time)-(end_time-start_time)%60)))
