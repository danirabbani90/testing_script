# %%
import os
import sys
sys.path.append(os.path.abspath(os.path.join(__file__,"../")))
sys.path.append(os.path.abspath(os.path.join(__file__,"../..")))
from frames_extraction import extract
from video_generation import video
import cv2
import numpy as np
import json
import re
import pandas as pd
import paths
import shutil
#%%
def load_json(json_file):
    with open(json_file) as f:
        file = json.load(f)
    return file

# %%
def label_frames(video_name,extract_path,labeled_path,results_path,df,fps=30):
    ext='.mp4'
    if os.path.exists(labeled_path):
      shutil.rmtree(labeled_path)
    os.makedirs(labeled_path)
    images_list = list(paths.list_images(extract_path))
    images_list.sort(key=lambda f: int(re.sub('\D', '', f)))
    video_name=video_name.split('.')[0]+'_labeled'+ext
    for image_path in images_list:
        img_path=os.path.split(image_path)[-1]
        print(img_path)
        img=cv2.imread(image_path)
        text=df.loc[img_path][0]
        x1,y1=img.shape[0:2]
        x1,y1=int(x1/3),int(y1/7)
        cv2.putText(img,text,(x1,y1),cv2.FONT_HERSHEY_SIMPLEX,2, (0,255,0), 2)
        cv2.imwrite(os.path.join(labeled_path,'{}'.format(img_path)),img)
    video(labeled_path,results_path,video_name,fps)
# %%
if __name__=='__main__':
    fps=15
    video_name='IMG_0876_60.mp4'
    csv_file_path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'results',video_name.split('.')[0]+'_actions_random_forest.csv')
    path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'data','testing_videos')
    file_path=os.path.join(path,video_name)
    extract_path=os.path.join(os.path.abspath(os.path.join(__file__,"../..")),'extracted_frames')
    labeled_path=os.path.join(os.path.abspath(os.path.join(__file__,"../..")),'detected_frames')
    results_path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'results')
    extract(file_path,extract_path)  
    df=pd.read_csv(csv_file_path,index_col=0)
    label_frames(video_name,extract_path,labeled_path,results_path,df,fps)
# %%
