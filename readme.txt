1- $ pip install -r requirements.txt

	i- In case of error, try:

				a) $ python -m pip install -U pip
				b) $ sudo apt install python3-dev

	ii- In case of configspace installation error, try:

				a) $ pip install pip==9.0.1 		


2- $ sudo apt install curl

3- To download and unzip the trained model (only for the first time):

				a) $ cd testing_scripts
				b) $ chmod +x download_model.sh
				c) $ ./download_model.sh

4- Put some relevent video in the 'data/testing_videos' folder.


5- $ cd testing_scripts			# change the directory

6- First step is to calibrate the pixels. Open video_segmentation.sh and add video name after -v and standard height of the athlete in inches after -sh. 

	Run the script by : 		a) $ chmod +x video_segmentation.sh
					b) $ ./video_segmentation.sh

	This will generate a json file informing if the calibration is successful (and inches per pixels,lowest point and x-mean) or unsuccessful.

7- The next step is to evaluate any given video. Open evaluate_video.sh, adding video name after -v, frames-per-second -fps, player's handedness -hand and inches-per-pixel -ipp.

	Run the script by : 		a) $ chmod +x evaluate_video.sh
					b) $ ./evaluate_video.sh			


8- Once the execution is complete, four JSON files will be generated, which can be accessed from the 'results' folder.

9-	i) In results/detected frames, you can view the calibrated area frames for the action video being run.
	ii) In results/segmented_frames, you can view segmented frames as well as bounding boxes being used for inches per pixel mapping.
	
 






