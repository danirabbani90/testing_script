#%%
import numpy as np
import pandas as pd
from scipy import ndimage, misc
import os
import sys
#%%
def calculate_max_min(df):
    max_val=df.to_numpy().max()
    min_val=df.to_numpy().min()
    return max_val, min_val
#%%
def min_max_scaling(df,max_val,min_val):
    df=(df-min_val)/(max_val-min_val)
    return df

# %%
def calculate(df,classes,inches_in_pixel):
    inches=1.595
    pixels=inches/inches_in_pixel
    frame_capture={}
    frame_status=np.ones(len(classes))*-1 
    df['frames']=range(len(df))
    vec1=df[[df.columns[5],'frames']]
    low_y=df[df.columns[7]].argmin()
    high_y=df[df.columns[7]].argmax()
    d=vec1.iloc[low_y:high_y+1,]
    d=d[d[d.columns[0]]!=-1]
    if len(d)!=0:
        dsub=abs(d[d.columns[0]].pct_change())/(d[d.columns[1]]-d[d.columns[1]].shift(1))
        dsub=dsub.dropna(axis=0)
        sigma=1.5

        q25, q75 = np.percentile(dsub, 25), np.percentile(dsub, 75)
        iqr = q75 - q25
        cut_off = iqr * 20
        lower, upper = q25 - cut_off, q75 + cut_off
        outliers = [x for x in dsub if x < lower or x > upper]
        outliers=np.array(outliers)
        outliers=outliers[outliers!=0]
        d=vec1.iloc[low_y:,:]
        if outliers.shape[0]!=0:
            outlier_in_wrist=dsub[dsub==outliers[0]].index.tolist()
            print(outlier_in_wrist)
            d=d.drop(index=outlier_in_wrist)
        d=d[d[d.columns[0]]!=-1]
        dsub=abs(d[d.columns[0]]-d[d.columns[0]].shift(1))/(d[d.columns[1]]-d[d.columns[1]].shift(1))
        dsub=dsub.dropna(axis=0)

    else:
        d=vec1.iloc[low_y:,]
        d=d[d[d.columns[0]]!=-1]
        dsub=abs(d[d.columns[0]]-d[d.columns[0]].shift(1))/(d[d.columns[1]]-d[d.columns[1]].shift(1))
        dsub=dsub.dropna(axis=0)

    if len(dsub[dsub>11])!=0:
        ball_release=dsub[dsub>pixels].index[0]
    else:
        q25, q75 = np.percentile(dsub, 25), np.percentile(dsub, 75)
        iqr = q75 - q25
        # calculate the outlier cutoff
        cut_off = iqr * np.median(dsub)*13.5
        lower, upper = q25 - cut_off, q75 + cut_off
        outliers = [x for x in dsub if x > upper]

        outliers=np.array(outliers)
        outliers=outliers[outliers!=0]
        if outliers.shape[0]!=0: 
            # ball_release=res_1[res_1==outliers[0]].index.tolist()[0]
            ball_release=dsub[dsub==outliers[0]].index.tolist()[0]
        else:
            # vec=df[df.columns[3]]
            # ball_release=vec.iloc[low_y:].idxmax()
            res_1 = abs(ndimage.gaussian_laplace(d[d.columns[0]], sigma=5))
            res_1=pd.Series(res_1,index=d.index)
            res_1=res_1.pct_change()
            res_1=res_1.dropna()
            res_1=res_1[res_1>0]
            ball_release=res_1.idxmax()
    print('ball_release:',ball_release)
    br_arg=int(ball_release.split('.')[0].split('_')[1])
    #%%
    sp=df[df.columns[0]]
    if len(sp.iloc[:low_y])!=0:
        sp_2=sp.iloc[:low_y]
    else:
        sp_2=sp
    set_position=sp_2.idxmax()
    sp_arg=sp_2.argmax()
    
    if sp.iloc[np.where(sp[set_position:]<0.6)].shape[0]!=0:
        # fm_arg=(np.where(sp[set_position:]<0.6)+sp_arg)[0][0]
        forward_motion=sp.iloc[np.squeeze(np.where(sp[set_position:]<0.6)+sp_arg,axis=0)].index[0]
    else:
        fm=df[df.columns[1]]
        forward_motion=fm.idxmax()
    # print('forward_motion:',forward_motion)
    fm_arg=int(forward_motion.split('.')[0].split('_')[1])


    vec2=df[df.columns[6]]
    vec2=vec2.dropna()
    try:
        if fm_arg>low_y:
            d=vec2.iloc[fm_arg:br_arg-1]
        else:
            d=vec2.iloc[low_y:br_arg-1,]
    except:
            d=vec2
    # foot_plant=d.idxmin()
    res_1 = abs(ndimage.gaussian_laplace(d, sigma=5))
    res_1=pd.Series(res_1,index=d.index)
    res_1=res_1.pct_change()
    res_1=res_1.dropna()
    res_1=res_1[res_1>0]
    try:
        foot_plant=res_1.idxmax()
    except:
        d=vec1.iloc[low_y:,0]
        d=d.reindex(index=d.index[::-1])
        d=d[d!=-1]
        data=d.pct_change()
        data=data.dropna()
        frame=data[data>0].index[0]
        frame,ext=frame.split('.')
        f,n=frame.split('_')
        br_arg=int(n)+1
        ball_release='{}_{}.{}'.format(f,br_arg,ext)
        br_arg=int(ball_release.split('.')[0].split('_')[1])
        try:
            if fm_arg>low_y:
                d=vec2.iloc[fm_arg:br_arg-1]
            else:
                d=vec2.iloc[low_y:br_arg-1,]
        except:
                d=vec2
    res_1 = abs(ndimage.gaussian_laplace(d, sigma=5))
    res_1=pd.Series(res_1,index=d.index)
    res_1=res_1.pct_change()
    res_1=res_1.dropna()
    res_1=res_1[res_1>0]
    foot_plant=res_1.idxmax()
    print('foot_plant:',foot_plant)
    fp_arg=int(foot_plant.split('.')[0].split('_')[1])
    # print('set_position:',set_position)
    # if br_arg<fp_arg:
    #     d=vec2.iloc[fm_arg:br_arg-1]
    
    frame_capture[set_position]=classes[0]
    frame_capture[forward_motion]=classes[1]
    frame_capture[foot_plant]=classes[2]
    frame_capture[ball_release]=classes[3]
    
    if len(frame_capture)<len(classes):
        a=set(frame_capture.values())
        b=set(classes)
        diff=b.difference(a)
        print(frame_capture)
        print('{} is not recognized, problems in the video or input data'.format(diff))
        print('terminating.....')
        sys.exit()

    frame_status[0]=int(set_position.split('.')[0].split('_')[1])
    frame_status[1]=int(forward_motion.split('.')[0].split('_')[1])
    frame_status[2]=int(foot_plant.split('.')[0].split('_')[1])
    frame_status[3]=int(ball_release.split('.')[0].split('_')[1])

    print(frame_capture)
    return frame_capture,frame_status
#%%
if __name__=='__main__':
    
    # csv_file_name='IMG_0881_best_hybrid_model_vgg19_prediction_parallel.csv'
    # csv_file_name='IMG_0552_2_best_hybrid_model_vgg19_prediction_parallel.csv'
    csv_file_name='IMG_0875_best_hybrid_model_vgg19_prediction_parallel.csv'
    # csv_file_name='IMG_0876_60_best_hybrid_model_vgg19_prediction_parallel.csv'
    csv_files_path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'results')
    csv_file=os.path.join(csv_files_path,csv_file_name)
    df = pd.read_csv(csv_file,index_col=0) 
    classes=df.columns[:4]
    frame_capture,frame_status=calculate(df,classes)
    print(frame_capture)
    print(frame_status)
# %%

