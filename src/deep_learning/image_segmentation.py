#%%
import os
import sys
import cv2
import imutils
import gc
sys.path.append(os.path.abspath(os.path.join(__file__,"/..")))
from gluoncv import model_zoo, data
from gluoncv.data.transforms.pose import detector_to_alpha_pose, heatmap_to_coord_alpha_pose
import mxnet as mx
import numpy as np
from position_calculations import calculate_max_area, scale_coordinates
from gluoncv.utils.viz import get_color_pallete
import time
#%%

#%%
class segmentation:
    def __init__(self):
        self.net=self.get_models()
        self.class_index=15
        self.thresh=6
        self.ext='.png'
    def get_models(self):
        net = model_zoo.get_model('deeplab_resnet101_coco', pretrained=True, ctx=mx.gpu(0))
        return net
    def person_segment(self,full_path,crop_size):
    	
        start=time.time()
        # original_height,original_width=crop_size
        image=cv2.imread(full_path)
        original_height,original_width=image.shape[0],image.shape[1]
        dim=[original_height,original_width]
        short=dim[np.argmin(dim)]//2
        new_height=original_height//3
        print('image shape',image.shape)
        image = mx.ndarray.array(image)
        image=image_resize(image,new_height)
        target_path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'results','segmented_frames')


        img = data.transforms.presets.segmentation.test_transform(image,ctx=mx.gpu(0))
        print('img shape',img.shape)
        
        #img, _ = data.transforms.presets.rcnn.transform_test(image,short=256)
        #img, _ = data.transforms.presets.rcnn.load_test(full_path,short=short)
        output = self.net.predict(img)
        end=time.time()
        print('time in prediction',end-start)
        start=time.time()
        try:
            predict = mx.nd.squeeze(output[:,self.class_index]).asnumpy()
        except mx.MXNetError:
            cX, cY=[],[]
            return cX, cY
        p=np.where(predict<self.thresh,0,1)
        p=p.astype('uint8')
        cnts = cv2.findContours(p.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        mask = np.ones(p.shape[:2], dtype="uint8") * 255
        # loop over the contours
        max_area_cnt=max(cnts,key=cv2.contourArea)
        max_area=cv2.contourArea(max_area_cnt)
        for c in cnts:
            # if the contour is bad, draw it on the mask
            cnt_area=cv2.contourArea(c)
            if cnt_area<max_area:
                cv2.drawContours(mask, [c], -1, 0, -1)
            # remove the contours from the image and show the resulting images
        img = cv2.bitwise_and(p,p, mask=mask)
        print('img shape',img.shape)
        print('image shape',image.shape)
        cnts = cv2.findContours(img, cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        if len(cnts)>1:
            print('====MORE THAN ONE CONTOUR DETECTED====')
        for c in cnts:
            M = cv2.moments(c)
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
        x,y,w,h = cv2.boundingRect(max_area_cnt)
        #image=img
        mask = get_color_pallete(img, 'coco')
        print('img shape',img.shape)
        ha=img.shape[0]/original_height
        wa=img.shape[1]/original_width
        image=image_resize(image,original_height)
        image=image.asnumpy()
        x1,y1,w1,h1 =([x,y,w,h]/np.array([wa,ha,wa,ha])).astype(int)
        cX,cY=([cX,cY]/np.array([wa,ha]))
        # x1,y1,w1,h1=np.array([x,y,w,h]).astype(int)
        lowest_point=h1+y1
        # lowest_point=int(h+y)

        del img
        gc.collect()
        end=time.time()
        print('time in processing',end-start)
        return cX,cY,lowest_point,mask
# %%
#%%
def image_resize(image,new_height):
    h,w=image.shape[:2]
    height_percent=new_height/h
    new_width=int(height_percent*w)
    image=data.transforms.image.imresize(image, w=new_width, h=new_height)
    return image