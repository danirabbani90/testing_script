#%%
import argparse
#%%
def getArgs(argv=None):
    parser = argparse.ArgumentParser(description="Parser for videosegmentation.py",)

    parser.add_argument("-v", "--video", type=str, default='IMG_6891.mov',
	                    help="Name of the input video along with the extension")

    parser.add_argument("-id","--video_id", type=int, default=1234567,
                        help="Video ID of the video being used")

    parser.add_argument("-g", "--game_name", type=str, default='softball',
	                    help="The game being played in the given video")

    parser.add_argument("-hand", "--handedness", type=str, default='right_handed',
	                    help="Handedness of the player: Select either 'right_handed' or 'left_handed' ")

    parser.add_argument("-fps", "--frames_per_second", type=int, default=240,
                        help="Frames per Second during the capture of the video")
                                                        
    parser.add_argument("-d","--distance_from_camera", type=float, default=10.0,
                        help="Distance of the planted stick from the camera in feet")

    parser.add_argument("-ph","--person_height", type=float,default=60.5,
        		help="Height of the athlete in inches")

    parser.add_argument("-sw","--stick_width", type=float, default=2.0, 
                        help="Width of the stick in inches")
    
    parser.add_argument("-ipp","--inches_perpixel", type=float,default=0.2962021104667744,
                        help="Inches per pixel in the current setting. Calculated using the calibration video in previous step")
                        
    parser.add_argument("-lp","--lowest_point_calc", type=float,default=834.75,
                        help="Lowest Point Calculated using the calibration video in previous step")
                        
    parser.add_argument("-xm","--mean_calc", type=float,default=897.731707317073,
                        help="X -Mean Calculated using the calibration video in previous step")

    return parser.parse_args(argv)
