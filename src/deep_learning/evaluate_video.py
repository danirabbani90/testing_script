#%%
import os
import sys
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.abspath(os.path.join(__file__,"../..")))
from utils.frames_extraction import extract
import cv2
import numpy as np
from position_calculations import display_angle,wrist_calculation,arm_displacement,max_velocity,ball_velocity,find_displacement
from pose_estimation import pose_model
from action_recognition import action_model
from image_segmentation import segmentation
from calculate_action import calculate
from skeleton.pose_processor import load_dataframe,calculate_distance,concatenate,calculate_max_min
import json
import re
import pandas as pd
from utils import paths
# from evaluation import classes,key_points
import time
import gc
import concurrent.futures
import multiprocessing
from multiprocessing import Process
import shutil
import argparse
import parser_arguments

#%%
def load_json(json_file):
    with open(json_file) as f:
        pose = json.load(f)
    return pose
    
#%%
def generate_json(data,json_path):
    if os.path.exists(json_path):
        os.remove(json_path)
    with open(json_path, 'w') as fp:
        print('Writing JSON file  ',json_path)
        json.dump(data, fp,indent=2)
    print('JSON file generated !')
#%%
ball_diameter=3.81      # inches
extract_path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'data','extracted_frames')
cropped_path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'results','detected_frames')
check_path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'data','check_frames')
if os.path.exists(cropped_path):
        shutil.rmtree(cropped_path)
os.makedirs(cropped_path)
if os.path.exists(check_path):
        shutil.rmtree(check_path)    
os.makedirs(check_path)
model_name="best_hybrid_model_vgg19"
csv_file_name='pose_filtered_dataframe.csv'
json_file_name='pose_filtered_coordinates.json'
model_path=os.path.join(os.path.abspath(os.path.join(__file__,"../..")),'models',model_name)
# Define the body keypoints for pose estimation 
key_points=['nose','left_eye','right_eye','left_ear','right_ear','left_shoulder','right_shoulder','left_elbow','right_elbow',
            'left_wrist','right_wrist','left_hip','right_hip','left_knee','right_knee','left_ankle','right_ankle']

csv_files_path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'results')
csv_file=os.path.join(csv_files_path,csv_file_name)
df=pd.read_csv(csv_file,index_col=0)
json_file=os.path.join(csv_files_path,json_file_name)
pose_train=load_json(json_file)
classes=df['label'].unique()
classes.sort()
print(classes)

allowed_fps=[30,60]
allowed_frames=dict(zip(allowed_fps,(130,250)))
validExts=(".mp4", ".mov", ".avi")

# %%
def error_response(fps,video_name):
    if fps not in allowed_fps:
        print('fps error. Please use allowed fps',allowed_fps)
        sys.exit()
    ext=video_name[video_name.rfind('.'):].lower()
    if not ext.endswith(validExts):
        print(f'Video format error. {ext} is not an allowed video format')
        print('Please use allowed video formats',validExts)
        sys.exit()
    

#%%
def process_image(full_path,BB,pose_data):
    img_path=os.path.split(full_path)[1]
    x1,y1,x2,y2=BB
    print(img_path)
    img=cv2.imread(full_path)
    image=img[y1:y2,x1:x2]
    cv2.imwrite(os.path.join(check_path,img_path), image)
    if image.shape[0]==0 or image.shape[1]==0:  
        print('--Person not found in ',img_path)
        center,local_lowest=[],[]
        predict=np.ones(len(classes))*-1
        del img,image
        gc.collect()
        return center,predict,local_lowest
    model=action_model(model_path)
    # position_text,predict_label=model.predict_action(image,classes)
    predict=model.predict_hybrid_action(pose_data,image)
    #print('Inside process_image')
    #print(f'predict for {img_path} is ',predict)
    del model       
    gc.collect()
    segment=segmentation()
    cX,cY,local_lowest=segment.person_segment(image)
    del segment,image     
    gc.collect()
    center=[int(cX),int(cY)]
    # return center,position_text,predict_label,y1+local_lowest
    return center,y1+local_lowest,predict
#%%
def pose_image(full_path):
    
    img_path=os.path.split(full_path)[1]
    print(img_path)
    pose=pose_model()
    # BB,pred_coords,index=pose.calculate_coordinates(full_path)
    BB,pred_coords,index=pose.calculate_coordinates(full_path)
    del pose
    gc.collect()
    key_maps=dict(zip(key_points,(pred_coords.astype(int)).tolist()))           # Create a dictionary of originalkeypoints

    pred_coords=np.array(pred_coords)                                           # convert coordinates to cropped image to feed to the model
    pred_coords[:,0]=pred_coords[:,0]-BB[0][0]
    pred_coords[:,1]=pred_coords[:,1]-BB[0][1]
    action_pose=dict(zip(key_points,(pred_coords.astype(int)).tolist()))        # action_pose should be fed to the action recognition model

    return BB,key_maps,action_pose,index
#%%
def evaluate_video(file_path,wrist,fps,inches_in_pixel,lowest_point,x_mean):
    test_file_name=os.path.split(file_path)[-1]
    lowest_point=int(lowest_point)
    x_mean=int(x_mean)
    print(file_path)
    print(extract_path)
    m=extract(file_path,extract_path)
    
    if m>allowed_frames[fps]:
        print(f'Total frames error. Video frames have exceeded allowed {allowed_frames[fps]} frames limit at fps {fps}')
        #sys.exit()

    if not os.path.exists(model_path):
        print('Action Recognition model not found. Please download the model using the script')
        sys.exit()
    json_files_path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'results',test_file_name.split('.')[0])
     
    wrist=wrist.split('_')[0]
    border_in_inches=10
    border_in_pixels=int(border_in_inches/inches_in_pixel)


    train_dfx,train_dfy,_=load_dataframe(pose_train,wrist)
    train_distance,_=calculate_distance(train_dfx,train_dfy)
    max_dfx,min_dfx=calculate_max_min(train_dfx)
    max_dfy,min_dfy=calculate_max_min(train_dfy)
    max_distance,min_distance=calculate_max_min(train_distance)



    pose_coordinates={}
    frame_status=np.ones(len(classes))*-1
    frame_capture={}
    output={}
    centers_dict={}
    ball_center={}
    ball=[]
    ball_width=[]
    ball_height=[]
    bounding_boxes=[]
    frame_dict={}

    prev_x,prev_y=None,None
    images_list = list(paths.list_images(extract_path))
    images_list.sort(key=lambda f: int(re.sub('\D', '', f)))
    #images_list=images_list[:4]
    # for i,image in enumerate(images_list):
    #     BB,pred_coords,index=pose_image(image)
    #     bounding_boxes.append(BB)
    total_workers=multiprocessing.cpu_count()
    wf=0.5
    print('Total processors : ', int(total_workers*wf))
    with concurrent.futures.ProcessPoolExecutor(max_workers=int(total_workers*wf)) as executor:
        print('Starting ProcessPoolExecutor for pose estimation')
        results=executor.map(pose_image,images_list)
    print('ProcessPoolExecutor completed successfully for pose estimation')

    pose_data=[]
    # ball_index=[]
    person_area=[]
    wrist_ball_dist=[]
    ankle_coord=[]
    results_check=[]
    if wrist=='right':
    		ankle='left_ankle'
    else:
    		ankle='right_ankle'


    for i,result in enumerate(results):
        img_path=os.path.split(images_list[i])[-1]
        print(img_path)

        BB=result[0]
        key_maps=result[1]      # original frame's poses
        action_pose=result[2]   # cropped image's poses
        index=result[3]

        x1,y1,x2,y2=BB[0]
        width=int(x2-x1)
        height=int(y2-y1)
        person_area.append(height*width)

        bounding_boxes.append(list(BB[0]))
        ankle_coord.append(key_maps[ankle])


        img=cv2.imread(images_list[i])
        img[lowest_point-border_in_pixels:lowest_point+border_in_pixels,0:img.shape[1],1]=img[lowest_point-border_in_pixels:lowest_point+border_in_pixels,0:img.shape[1],1]+np.ones([(lowest_point+border_in_pixels)-(lowest_point-border_in_pixels),(img.shape[1]-0)])+40
        if index[1]>-1:  # Ball exists
                x1b,y1b,x2b,y2b=BB[1]
                print(BB[1])
                x=int(x1b+((x2b-x1b)//2))
                y=int(y1b+((y2b-y1b)//2))

                wrist_ball_dist.append(np.sqrt(((key_maps['{}_wrist'.format(wrist)][0]-x)**2 + ((key_maps['{}_wrist'.format(wrist)][1]-y)**2))))

                ball_center[img_path]=x,y
                ball.append([x,y])
                ball_width.append(abs(x2b-x1b))
                ball_height.append(abs(y2b-y1b))
                cv2.circle(img,(x,y),1,(0,0,255), 2)
                cv2.circle(img,(key_maps[ankle][0],key_maps[ankle][1]),1,(0,0,255),2)
        else:
                ball.append(-1)
                ball_width.append(-1)
                ball_height.append(-1)
                wrist_ball_dist.append(-1)



        cv2.ellipse(img,(int(x_mean),lowest_point),(15,10),0,0,180,(0,0,255),-1)
        cv2.imwrite(os.path.join(cropped_path,img_path), img)
        #print(ball_center)
        #print(ball)
        pose_coordinates[img_path]=key_maps 

        action_pose['label']=np.nan
        poses={}
        poses[img_path]=action_pose   
        df_x,df_y,_=load_dataframe(poses,wrist)
        df_distance,df_angle=calculate_distance(df_x,df_y)
        df_pose=concatenate(df_x,df_y,df_distance,df_angle,max_dfx,min_dfx,max_dfy,min_dfy,max_distance,min_distance)
        # print(df_pose)
        pose_data.append(df_pose.values)


    #print(bounding_boxes) 
    generate_json(pose_coordinates,json_files_path+'_coordinates.json')
    with concurrent.futures.ProcessPoolExecutor(max_workers=int(total_workers*wf)) as executor:
        print('Starting ProcessPoolExecutor for action recognition')
        results=executor.map(process_image,images_list,bounding_boxes,pose_data)
    print('ProcessPoolExecutor completed successfully for action recognition')
    for i,result in enumerate(results):
        img_path=os.path.split(images_list[i])[-1]
        print(img_path)


        center,local_lowest=result[0],result[1]
        results_check.append(result[2])
        # center,position_text,predict_label,local_lowest=result[0],result[1],result[2],result[3]
        print('local_lowest point:',local_lowest)
        print('Upper boundary:',lowest_point-border_in_pixels)
        print('Lower boundary:',lowest_point+border_in_pixels)
        if local_lowest<(lowest_point-border_in_pixels):
	        print('Player outside upper boundary')
        elif local_lowest>(lowest_point+border_in_pixels):
        	print('Player outside lower boundary')
        elif (center[0]==[] or center[1]==[]):
            print('Segmentation error: Center not found in ',img_path)
            continue
        else:                    
            centers_dict[img_path]=np.add(bounding_boxes[i][0:2],center).astype(int).tolist()

    generate_json(centers_dict,json_files_path+'_center_of_mass.json')
    results_check=np.array(results_check)
    print('results_check shape is:',results_check.shape)
    try:
    	results_check=np.squeeze(results_check,axis=1)
    except:
        print('======ERROR in results_check=======')
        print(results_check)

    images_path=os.listdir(extract_path)
    images_path.sort(key=lambda f: int(re.sub('\D', '', f)))

    df=pd.DataFrame(results_check,columns=classes,index=images_path)
    # print(person_area)
    # print(ball_index)
    df2=pd.DataFrame(person_area,columns=['area'],index=images_path)
    df3=pd.DataFrame(wrist_ball_dist,columns=['{}_wrist_ball_dist'.format(wrist)],index=images_path)
    res = [val[ankle] for key, val in pose_coordinates.items() if ankle in val]
    frames_dict=dict(zip(pose_coordinates.keys(),res))
    df4=find_displacement(frames_dict,ankle)
    
    res = [val[wrist+'_wrist'] for key, val in pose_coordinates.items() if wrist+'_wrist' in val]
    frames_dict=dict(zip(pose_coordinates.keys(),res))
    df5=pd.DataFrame.from_dict(frames_dict,orient='index',columns=['x','wrist_y_coordinates'])
    df5=df5.drop(columns='x')
    df_final=pd.concat([df,df2,df3,df4,df5], axis=1)
    csv_file=os.path.join(json_files_path+'_'+model_name+'_prediction_parallel.csv')
    df_final.to_csv(csv_file)

    frame_capture,frame_status=calculate(df_final,classes)

    for img_path,label in frame_capture.items():
        lean_angle,lean=display_angle(pose_coordinates[img_path],wrist)
        if lean=='forward':
            output['forward_lean_angle_'+'_'.join(label.split('_')[1:])]=str(lean_angle) + ' degrees'
            output['backward_lean_angle_'+'_'.join(label.split('_')[1:])]=np.nan
        else:
            output['backward_lean_angle_'+'_'.join(label.split('_')[1:])]=str(lean_angle) + ' degrees'
            output['forward_lean_angle_'+'_'.join(label.split('_')[1:])]=np.nan

    _,_,w=wrist_calculation(pose_coordinates,wrist=wrist)
    frame_capture,_=arm_displacement(w,frame_capture)
    frame_max_velocity, max_v,max_d=max_velocity(centers_dict,inches_in_pixel,fps)
    if frame_max_velocity in frame_capture.keys():
        keys=frame_capture[frame_max_velocity]
        frame_capture[frame_max_velocity]=['Frame of maximum velocity',str(max_v)+' in/sec',str(max_d)+' inches',keys]
    else:
        frame_capture[frame_max_velocity]=['Frame of maximum velocity',str(max_v)+' in/sec',str(max_d)+' inches']
    
    lean_angle,lean=display_angle(pose_coordinates[frame_max_velocity],wrist)
    if lean=='forward':
        output["forward_lean_angle_max_velocity"]=str(lean_angle) + ' degrees'
        output["backward_lean_angle_max_velocity"]=np.nan
    else:
        output["backward_lean_angle_max_velocity"]=str(lean_angle) + ' degrees'
        output["forward_lean_angle_max_velocity"]=np.nan
    _,text=arm_displacement(w,{frame_max_velocity:frame_capture[frame_max_velocity]})
    output["arm_position_at_max_velocity"]=text
    max_v=0.05681*max_v
    output["top_velocity"]=str(max_v)+' miles per hour'
    frame_classes=dict(zip(classes,frame_status))

    total_pitch_frames=abs(frame_classes[classes[3]]-frame_classes[classes[0]])
    output["pitch_duration"]=str(total_pitch_frames/fps) + ' seconds'

    stride_velocity_frames=abs(frame_classes[classes[2]]-frame_classes[classes[1]])
    stride_time=stride_velocity_frames/fps
    diff_x=(centers_dict['frame_{}.jpg'.format(int(frame_classes[classes[2]]))][0]-centers_dict['frame_{}.jpg'.format(int(frame_classes[classes[1]]))][0])*inches_in_pixel
    output["Stride velocity"]=str(0.05681*abs(diff_x)/stride_time) + ' miles per hour'
    output["shoulder_hip_joint_frames_file"]=json_files_path

    # Ball Velocity
    

    release_frame=int(frame_classes[classes[3]])
    ball_coordinates=[[i,x] for i,x in enumerate(ball[release_frame:]) if x!=-1]
    width=[x for x in ball_width[release_frame:] if x!=-1]
    height=[x for x in ball_height[release_frame:] if x!=-1]
    inches_per_pixel=ball_diameter/np.mean([height,width],axis=0)
    velocity,release_angle=ball_velocity(ball_coordinates,inches_in_pixel,fps,inches_per_pixel)
    print('inches_in_pixel',inches_in_pixel)
    print('inches_per_pixel',inches_per_pixel)
    print(velocity)
    print(release_angle)
    velocity=0.05681*velocity
    output['Inches per pixel']=inches_in_pixel
    output['Ball Velocity']=str(velocity)+' miles per hour'
    output['Ball Angle']=str(release_angle)+' degrees'
    generate_json(output,json_files_path+'_output.json')
    generate_json(frame_capture,json_files_path+'.json')
#%%
if __name__=='__main__':
    start_time=time.time()
    args = parser_arguments.getArgs(sys.argv[1:])
    video_name=args.video
    #video_id='1234567'
    fps=args.frames_per_second
    game_name='softball'
    wrist=args.handedness
    inches_in_pixel=args.inches_perpixel           # (inches)
    lowest_point=args.lowest_point_calc
    x_mean=args.mean_calc 
    path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'data','testing_videos')
    file_path=os.path.join(path,video_name)
    error_response(fps,video_name)
    evaluate_video(file_path,wrist,fps,inches_in_pixel,lowest_point,x_mean)
    end_time=time.time()
    print('Total execution time is : %d minutes and %d seconds'%(((end_time-start_time)-(end_time-start_time)%60)//60,(end_time-start_time)-((end_time-start_time)-(end_time-start_time)%60)))
# %%
