#%%
import cv2
import os
import sys
import shutil
#%%
def extract(file_path,extract_path):
    if os.path.exists(extract_path):
      shutil.rmtree(extract_path)
    os.makedirs(extract_path)    
    if not os.path.exists(file_path):
        print("Video not found {}".format(file_path))
        sys.exit()
    video = cv2.VideoCapture(file_path)
    print("camera opened: ",video.isOpened())
    m=0
    check=-1
    while (video.isOpened):
        a,image=video.read()
        if a:
          if image.size!=0:
            check=1
            cv2.imwrite(os.path.join(extract_path,'frame_{}.jpg'.format(m)),image)
            m=m+1
          if cv2.waitKey(0) & 0xFF==ord('q'):
              break
        else:
          break
    video.release()
    cv2.destroyAllWindows()
    if check>0:
      print('Video successfully decomposed into {} frames in "{}"'.format(m,extract_path))
      return m
    else:
      print('Video decomposition failed. Please check the video source at "{}"'.format(file_path))
      sys.exit()
# %%
if __name__=='__main__':
  video_name='edited_IMG_0836.mp4'
  path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'data','testing_videos')
  file_path=os.path.join(path,video_name)
  extract_path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'data','extracted_frames')
  extract(file_path,extract_path)
