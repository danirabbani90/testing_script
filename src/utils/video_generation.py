#%%
import os
import cv2
import re
#%%
def video(detected_frames,video_path,video_name,fps):

      images_list=os.listdir(detected_frames)
      images_list.sort(key=lambda f: int(re.sub('\D', '', f)))
      frame_array = []
      for file in images_list:
            file_path=os.path.join(detected_frames,file)
            img = cv2.imread(file_path)
            if img is None:
                  continue
            height, width, layers = img.shape
            size = (width,height)
            frame_array.append(img)
      path_out=os.path.join(video_path,video_name) 
      out = cv2.VideoWriter(path_out,cv2.VideoWriter_fourcc(*'mp4v'), fps, size)
      for i in range(len(frame_array)):
            out.write(frame_array[i])
      out.release()
      print('Video generation complete\n')
      print ('video path final is:',path_out)
# %%
if __name__=='__main__':

      video_name='IMG_0875_calibrated.mp4'
      path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'data','testing_videos')
      extract_path=os.path.join(os.path.abspath(os.path.join(__file__,"../../..")),'results','detected_frames')
      fps=60
      video(extract_path,path,video_name,fps)
